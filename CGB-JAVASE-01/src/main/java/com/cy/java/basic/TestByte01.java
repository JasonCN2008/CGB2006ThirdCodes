package com.cy.java.basic;

public class TestByte01 {
    //JVM 参数：
	//最小堆:-Xms5m
	//最大堆:-Xmx5m
	public static void main(String[] args) {
		byte array1[]=new byte[1024*1024];//1M
		byte array2[]=new byte[1024*1024];
		byte array3[]=new byte[1024*1024];
		byte array4[]=new byte[1024*1024];
		byte array5[]=new byte[1024*1024];
		//OutOfMemoryError: Java heap space
	}
}
