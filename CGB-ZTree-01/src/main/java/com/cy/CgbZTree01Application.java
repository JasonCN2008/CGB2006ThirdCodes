package com.cy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CgbZTree01Application {

	public static void main(String[] args) {
		SpringApplication.run(CgbZTree01Application.class, args);
	}

}
