package com.cy.pj.goods.dao;
//包结构的构成:
//公司域名:com.cy
//项目名:pj
//模块名:goods
//哪一层:dao

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cy.pj.goods.pojo.Goods;

@Mapper
public interface GoodsDao {//这个接口在运行时系统底层会产生其实现类,但没有源码(只有字节码).
	
	 @Update("update tb_goods set name=#{name},remark=#{remark} where id=#{id} ")
	 int updateGoods(Goods goods);
	
	 @Insert("insert into tb_goods(name,remark,createdTime) values (#{name},#{remark},#{createdTime})")
	 int insertGoods(Goods goods);

	 @Select("select id,name,remark,createdTime from tb_goods where id=#{id}")
	 Goods findById(Integer id);
	 
	 @Select("select id,name,remark,createdTime from tb_goods")
	 List<Goods> findGoods();
	 
	 @Delete("delete from tb_goods where id=#{id}")
	 int deleteById(Integer id);
}









