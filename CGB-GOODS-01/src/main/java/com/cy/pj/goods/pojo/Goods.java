package com.cy.pj.goods.pojo;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

//@AllArgsConstructor //添加基于全部属性构建的构造函数
//@NoArgsConstructor //添加无参构造函数
@ToString
@Getter
@Setter
//@Data //添加此注解会在Goods类中自动添加set,get,toString等方法
public class Goods {//Goods.class
	private Long id;//id bigint primary key auto_increment
	private String name;//name varchar(100) not null
	private String remark;//remark text
	private Date createdTime;//createdTime datetime
}//lombok底层基于字节码技术为类添加相关方法或属性


