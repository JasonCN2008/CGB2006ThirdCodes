package com.cy.pj.goods.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.pj.goods.dao.GoodsDao;
import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GoodsServiceImpl implements GoodsService {
	@Autowired
	private GoodsDao goodsDao;

	//private static final Logger log=LoggerFactory.getLogger(GoodsServiceImpl.class);
	
	@Override
	public List<Goods> findGoods() {
		long t1=System.currentTimeMillis();
		List<Goods> list=goodsDao.findGoods();
		long t2=System.currentTimeMillis();
		log.info("findGoods().time:{}",(t2-t1));
		return list;
	}

	
	@Override
	public Goods findById(Integer id) {
		return goodsDao.findById(id);
	}
	
	@Override
	public int updateGoods(Goods goods) {
		// TODO Auto-generated method stub
		return goodsDao.updateGoods(goods);
	}
	@Override
	public int saveGoods(Goods goods) {
		// TODO Auto-generated method stub
		goods.setCreatedTime(new java.util.Date());
		return goodsDao.insertGoods(goods);
	}
	
	@Override
	public int deleteById(Integer id) {
		try{Thread.sleep(5000);}catch(Exception e) {}
        //.....
		return goodsDao.deleteById(id);
	}
	

}



