package com.cy.pj.goods.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cy.pj.goods.pojo.Goods;
import com.cy.pj.goods.service.GoodsService;
@Controller //@Service,@Component
@RequestMapping("/goods/")
public class GoodsController {
	//has a+DI
	@Autowired
	private GoodsService goodsService;
	
	
	@RequestMapping("doFindById/{id}")
	public String doFindById(@PathVariable Integer id,Model model) {
		Goods goods=goodsService.findById(id);
		model.addAttribute("goods",goods);
		return "goods-update";
	}
	
	@RequestMapping("doGoodsAddUI")
	public String doGoodsAddUI() {
		return "goods-add";
	}
	
	@RequestMapping("doSaveGoods")
	public String doSaveGoods(Goods goods) {//"name=aaa&&remark=aaaaaaaaa"
		goodsService.saveGoods(goods);
		return "redirect:/goods/doGoodsUI";
	}
	
	@RequestMapping("doUpdateGoods")
	public String doUpdateGoods(Goods goods) {
		goodsService.updateGoods(goods);
		return "redirect:/goods/doGoodsUI";
	}
	/**
	  *  基于商品id执行商品删除操作
	 * @param id 商品id,一定要与客户端传过来的参数id名相同.
	 * @return 重定向到doGoodsUI
	 * rest风格:一种软件架构编码风格,其设计的目的主要是在异构系统之间实现兼容(跨平台)
	 * rest风格的url的定义:{a}/{b}/{c},这里的a,b,c分别为变量
	  *   假如希望方法参数的值来自rest风格的url,
	  *   可以使用@PathVariable注解对方法参数进行描述(方法参数名需要和url中{}内部变量名相同)
	 */
	@RequestMapping("doDeleteById/{id}")
	public String doDeleteById(@PathVariable Integer id) {
		goodsService.deleteById(id);
		return "redirect:/goods/doGoodsUI";
	}//这里redirect表示重定向,后面的第一个"/"默认位置为localhost:port/context-path/
	
	@RequestMapping("doGoodsUI")
	public String doGoodsUI(Model model) {//ModelAndView中的model对象
		 //调用业务层方法获取商品信息
		 List<Goods> list=
		 goodsService.findGoods();
		 //将数据存储到请求作用域
		 model.addAttribute("goods", list);
		 return "goods";//viewname
		//FAQ:
		//1)返回的viewname会给谁?谁调用doGoodsUI方法就给谁(DispatcherServlet).
		//2)谁负责解析viewname?ViewResolver(ThymleafViewResolver)
		//3)viewname解析的结果会响应到哪里?(prefix+viewname+suffix会响应到客户端)
	}
	
}
