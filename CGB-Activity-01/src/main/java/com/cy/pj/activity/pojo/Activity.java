package com.cy.pj.activity.pojo;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class Activity {
	private Integer id;
	private String title;
	private String category;
	//@DateTimeFormat 作用于set方法表示为属性赋值，基于什么日期格式执行
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
	//@JsonFormat 用于告诉spring mvc 转换json时,将日期按照指定格式进行转换.
	//@JsonFormat 作用于对象get方法，表示获取值时的一种格式
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm",timezone = "GMT+8")
	private Date startTime;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm",timezone = "GMT+8")
	private Date endTime;
	
	private String remark;
	private Integer state=1;
	
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm",timezone = "GMT+8")
	private Date createdTime;
	private String createdUser;
}
