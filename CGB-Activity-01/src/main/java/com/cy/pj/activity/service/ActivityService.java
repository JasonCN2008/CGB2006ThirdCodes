package com.cy.pj.activity.service;

import java.util.List;

import com.cy.pj.activity.pojo.Activity;

public interface ActivityService {

	 int saveObject(Activity entity);
	 List<Activity> findActivitys();
}
