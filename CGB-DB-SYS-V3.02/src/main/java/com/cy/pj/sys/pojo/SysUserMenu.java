package com.cy.pj.sys.pojo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 封装用户菜单信息
 */
@Setter
@Getter
@ToString
public class SysUserMenu implements Serializable{
	private static final long serialVersionUID = -8089063957163137501L;
	private Integer id;
	private String name;
	private String url;
	private List<SysUserMenu> childs;
}
