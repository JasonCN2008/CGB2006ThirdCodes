package com.cy.pj.common.cache;

import org.springframework.stereotype.Component;

/**
 * FAQ?在SpringBoot工程中如何将一个类的实例交给spring创建和管理。
 * 	1)将类放在启动类所在包或子包中
 *  2)将类使用spring框架中指定注解进行描述，例如@Component
 */
@Component("defaultCache") //注解起的作用就是做标记
public class DefaultCache {

}
